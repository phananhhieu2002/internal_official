import styled from "styled-components";
import {Pagination} from "antd";
import {useState} from "react";
import type { PaginationProps } from 'antd';
import {setPage} from "@/redux/slices/app.slice";
import {useAppDispatch} from "@/redux/store";


const Styled = styled.div`
    .ant-pagination {
        float: right;
        margin-top: 20px;
        font-weight: 400 !important;
    }

    .ant-pagination-item-active {
        background-color: #1677ff;
    }

    .ant-pagination-item-active a {
        color: #ffffff;
    }
`
const PaginationComponent = ({total , item , current} : {total? : number , item? : number , current : number} ) => {
    const dispatch = useAppDispatch();
    const [currentPage , setCurrentPage] = useState<number>(current);
    const onChange : PaginationProps['onChange'] = (page) => {
        setCurrentPage(page);
        dispatch(setPage(page));
    }
    return (
        <Styled>
            <Pagination current={currentPage} defaultPageSize={item} onChange={onChange} total={total} />
        </Styled>
    )
}

export default PaginationComponent;