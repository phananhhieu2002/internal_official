const axiosConfigs = {
  development: {
    baseURL: 'https://stable-api.northstudio.dev/api/v1',
    timeout: 10000
  },
  production: {
    baseURL: 'https://stable-api.northstudio.dev/api/v1',
    timeout: 10000
  }
}
const getAxiosConfig = () => {
  const nodeEnv: NodeJS.ProcessEnv['NODE_ENV'] = process.env.NODE_ENV;
  // @ts-ignore
  return axiosConfigs[nodeEnv];
}

export const axiosConfig = getAxiosConfig();