import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/app-layout";
import MenuLayout from "src/layouts/menu-layout";
import React from "react";
import {MenuItem} from "@/@types/global";
import RegulationLayout from "src/layouts/regulation-layout";

function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: 'group',
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
        type,
    } as MenuItem;
}
const items : any = [
    getItem('Thưởng', '/hrm/payment-regulation/bonus'),
    getItem('Phạt', '/hrm/payment-regulation/foul'),
    getItem('Trợ cấp', '/hrm/payment-regulation/support'),
    getItem('Quy định khác', '/hrm/payment-regulation/rule'),
]
const PaymentRegulation : NextPageWithLayout = () => {
    return (
        <AppLayOut>
            <MenuLayout items={items} text="Thêm quy định" key="new">
                <RegulationLayout title="Thưởng" />
            </MenuLayout>
        </AppLayOut>
    )
}

export default PaymentRegulation;