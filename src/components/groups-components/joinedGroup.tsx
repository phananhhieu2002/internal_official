import styled from "styled-components";
import {useEffect, useRef, useState} from "react";
import {Group} from "@/@types/global";
import {Input, Row, Spin} from "antd";
import {SearchOutlined} from "@ant-design/icons";
import Utils from "@/utils";
import PaginationComponent from "@/components/share-components/Pagination";
import {RootState, useAppDispatch, useAppSelector} from "@/redux/store";
import GroupList from "@/components/groups-components/groupList";
import {getGroups} from "@/redux/actions/group";

const Styled = styled.div`
  .gropup_In {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 24px;
  }

  .gropup_In .part {
    font-size: 17px !important;
    font-weight: 700;
  }

  .ant-card {
    //border: 1px solid #e6ebf1 !important;
  }

  .ant-row {
    justify-content: space-between;
    align-items: center;
  }

  .ant-col {
    padding: 8px;
  }

  .mr-2 {
    margin-right: 5px;
  }

  .line_1 {
    border-bottom: 1px solid #e6ebf1;
    padding-bottom: 8px;
  }

  .group_joined {
    margin-top: 25px;
  }

  .ant-avatar {
    width: 40px;
    height: 40px;
  }

  .ant-card-body {
    padding: 15px 15px 24px !important;
  }

  .description {
    min-height: 130px;
    padding: 10px;
    align-items: flex-start !important;
    font-size: 16px;
    font-weight: 500;
    color: #455560;
  }

  .ant-tag {
    border-radius: 15px;
    height: 25px;
  }

  .group_joined .other h5 {
    font-size: 18px;
  }

  .group_joined .other span {
    font-weight: 400;
  }

  .line_1:hover {
    cursor: pointer;
  }

  .anticon {
    margin-right: 5px;
  }

  .ant-card {
    box-shadow: 0 0 2px #ccc !important;
  }

  .mr-2 {
    margin-right: 10px;
  }

  .input {
    font-weight: 500;
  }

  .ant-avatar img {
    width: 100% !important;
    height: 100% !important;
    border-radius: 100%;
    object-fit: cover;
  }

  .group_header {
    display: flex;
    //max-width: 188px;
  }

  .group_header_logo {
    display: flex;
    margin-right: 10px;
    align-items: center;
  }

  .group_header_info {
    display: flex;
    align-items: center;
  }

  .group_header_title {
    line-height: 1;
    font-weight: 650 !important;
    font-size: 15px !important;
  }

  .ant-tag {
    font-size: 12px;
  }

  .loading {
    width: 100%;
    display: flex;
    justify-content: center;
  }
`
const JoinedGroup = () => {
	// @ts-ignore
	const allgroup = useAppSelector((state) => state.group);
	const searchRef = useRef<any>(null);
	const pageCurrent = useAppSelector((state: RootState) => state.app.page);
	const dispatch = useAppDispatch();
	// @ts-ignore
	const [groups, setGroups] = useState<Group[]>(allgroup)
	const [keyword, setKeyword] = useState('');
	const [total, setTotal] = useState(0);
	const [currentPage, setCurrentPage] = useState(1);
	const [loading, setLoading] = useState(false);
	const getAllGroups = async (page = pageCurrent, slug = keyword, limit = 4, sortBy = '-createdAt') => {
		try {
			setLoading(true)
			const queries = {page, limit, sortBy};
			slug.trim().length > 0 && Object.assign(queries, {slug: Utils.slugify(slug)});
			// @ts-ignore
			// dispatch(getGroups(queries));
			dispatch(getGroups(queries))
		} catch (err) {
			console.log(err);
		}
	};
	useEffect(() => {
		getAllGroups();
	}, [pageCurrent]);
	useEffect(() => {
		setGroups(allgroup.data);
		// @ts-ignore
		setCurrentPage(allgroup.pagination.page);
		// @ts-ignore
		setTotal(allgroup.pagination.total);
		setLoading(false);
	}, [allgroup])
	return (
		<Styled>
			<div className="gropup_In">
				<span className="part">Nhóm đã tham gia</span>
				<Input
					size="small"
					className="input"
					placeholder="Tìm kiếm..."
					prefix={<SearchOutlined/>}
					onInput={(e: any) => {
						const value = e.target.value;
						setKeyword(value);
						if (searchRef.current) {
							clearTimeout(searchRef.current);
						}
						searchRef.current = setTimeout(() => {
							getAllGroups(1, value);
						}, 500);
					}}
				/>
			</div>
			<div className="group_joined">
				<Row>
					{
						!loading ? (<GroupList groups={groups}/>) :
							(
								<div className="loading">
									<Spin size="large"/>
								</div>
							)
					}
				</Row>
				<PaginationComponent current={pageCurrent} item={4} total={total}/>
			</div>
		</Styled>
	)
}

export default JoinedGroup;