import {NextPageWithLayout} from "@/pages/_app";
import styled from "styled-components";
import AppLayOut from "@/layouts/app-layout";
import MenuLayout from "src/layouts/menu-layout";
import {EditOutlined, UploadOutlined} from "@ant-design/icons";
import Head from "next/head";
import {Button, message, Select, Upload, UploadProps} from "antd";
import InputComponent from "@/components/share-components/Input";
import TextArea from "@/components/share-components/TextArea";
import {
	InboxOutlined,
	MailOutlined,
	FileTextOutlined,
	StarOutlined,
	DeleteOutlined
} from "@ant-design/icons";
import {MenuItem} from "@/@types/global";

function getItem(
	label: React.ReactNode,
	key: React.Key,
	icon?: React.ReactNode,
	children?: MenuItem[],
	type?: 'group',
): MenuItem {
	return {
		key,
		icon,
		children,
		label,
		type,
	} as MenuItem;
}

const items: any = [
	getItem('Hộp thư đến', '/mail/manage-mails/inbox', <InboxOutlined/>),
	getItem('Thư đã gửi', '/mail/manage-mails/sent', <MailOutlined/>),
	getItem('Bản thảo', '/mail/manage-mails/draft', <FileTextOutlined/>),
	getItem('Đã gắn sao', '/mail/manage-mails/starred', <StarOutlined/>),
	getItem('Đã xoá', '/mail/manage-mails/trash', <DeleteOutlined/>),
]
const ComposeContainer = styled.div`
  .form h4 {
    padding: 10px 0;
  }

  .ant-select {
    margin: 20px 0;
  }

  .ant-form-item {
    margin-top: 20px;
  }

  .toolButton {
    float: right;
  }
`
const Write: NextPageWithLayout = () => {
	const props: UploadProps = {
		name: 'file',
		action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
		headers: {
			authorization: 'authorization-text',
		},
		onChange(info) {
			if (info.file.status !== 'uploading') {
				console.log(info.file, info.fileList);
			}
			if (info.file.status === 'done') {
				message.success(`${info.file.name} file uploaded successfully`);
			} else if (info.file.status === 'error') {
				message.error(`${info.file.name} file upload failed.`);
			}
		},
	};
	return (
		<ComposeContainer>
			<Head>
				<title>Mail</title>
			</Head>
			<div className="form">
				<h4>Tin nhắn mới</h4>
				<Select
					style={{width: "100%"}}
					options={[]}
				/>
				<InputComponent label="" placeholderText="Subject:"/>
				<Upload {...props}>
					<Button icon={<UploadOutlined/>}>Click to Upload</Button>
				</Upload>
				<TextArea label=""/>
				<div className="toolButton">
					<Button type="link">Save Draft</Button>
					<Button style={{marginRight: 10}}>Discard</Button>
					<Button type="primary">Send</Button>
				</div>
			</div>
		</ComposeContainer>
	)
}
Write.getLayout = function getLayout(page) {
	return (
		<AppLayOut>
			<MenuLayout items={items} text="Soạn tin" icon={<EditOutlined/>} keyProp="/mail/manage-mails/compose">
				{page}
			</MenuLayout>
		</AppLayOut>
	)
}
export default Write;