import {ApiBaseService} from "@/service/base.service";

export class AuthService extends ApiBaseService {
  authConfig?: AuthConfig;

  appId = 'dfbfacb4-ccf6-4cdd-90d1-fe9602fdcdc7'
  authServer = 'https://auth.northstudio.vn'

  constructor() {
    super();
    if(typeof window !== "undefined") {
      this.authConfig = {
        headers: {
          Referer: window.location.hostname
        }
      }
    }
  }
  public async register(payload: any) {
    return (await this.httpClient.post("/auth/register", payload, this.authConfig))?.data;
  }

  generateAuthUrl() {
    return `${this.authServer}/oauth/authorize?response_type=code&client_id=${this.appId}&redirect_uri=${
      window.document.location.origin + '/auth/callback'
    }`
;
  }

  async requestToken(code: string, deviceToken?: string) {
    const {data} = await this.httpClient.post(`/auth/token`, {
      code,
      redirect_uri: window.document.location.origin + '/auth/callback',
      grant_type: 'authorization_code',
      device_token: deviceToken,
    }, this.authConfig)
    return data
  }

  public async login(payload: any) {
    return (await this.httpClient.post("/internal/auth/sign-in", payload, this.authConfig))?.data;
  }

  public async loginWithSSO(payload: any) {
    return (await this.httpClient.post("/auth/sso", payload, this.authConfig))?.data;
  }

  public async resendVerifyEmail(payload: any) {
    return (await this.httpClient.post("/auth/verify-account/new", payload, this.authConfig))?.data;
  }

  public async forgetPassword(payload: any) {
    return (await this.httpClient.post("/auth/forget_password", payload, this.authConfig))?.data;
  }


  public async logout(payload: any) {
    return (await this.httpClient.post("/auth/logout", payload, this.authConfig))?.data;
  }
}

export const authService = new AuthService();
