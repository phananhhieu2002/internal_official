import {createAsyncThunk} from "@reduxjs/toolkit";
import ApiService from "@/service/ApiService";

export const getGroups = createAsyncThunk('groups/get', async (queries: {
	page: number,
	slug: string,
	sortBy: string
}) => {
	const data = await ApiService.getGroups(queries);
	return data;
})

export const createNewGroup = createAsyncThunk('group/create', async (formData: any) => {
	const res = await ApiService.createGroup(formData);
	return res;
})

export const deleteGroup = createAsyncThunk('group/delete', async (id: string) => {
	await ApiService.deleteGroup(id);
	return id;
})