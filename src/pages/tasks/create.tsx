import AppLayOut from "@/layouts/app-layout";
import { NextPageWithLayout } from "../_app";
import styled from 'styled-components';
import type { MenuProps } from 'antd/es/menu';
import React from "react";
import TaskLayout from "src/layouts/task-layout";
import FormWrap from '../../components/tasks/TaskForm/index';

const MainContent = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`


const Create: NextPageWithLayout = () => {
    return (
        <MainContent>
            <FormWrap title="Tạo nhiệm vụ" />
        </MainContent>
    )
}
Create.getLayout = function getLayout(page) {
    return (
        <AppLayOut>
            <TaskLayout>
                {page}
            </TaskLayout>
        </AppLayOut>
    )
}

export default Create;