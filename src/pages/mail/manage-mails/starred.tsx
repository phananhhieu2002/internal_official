import styled from "styled-components";
import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/app-layout";
import MenuLayout from "src/layouts/menu-layout";
import {EditOutlined} from "@ant-design/icons";
import {Empty, Input, Row, Select} from "antd";
import Head from "next/head";
import {
	InboxOutlined,
	MailOutlined,
	FileTextOutlined,
	StarOutlined,
	DeleteOutlined
} from "@ant-design/icons";
import {useSelector} from "react-redux";
import {RootState} from "@/redux/store";
import {MenuItem} from "@/@types/global";

function getItem(
	label: React.ReactNode,
	key: React.Key,
	icon?: React.ReactNode,
	children?: MenuItem[],
	type?: 'group',
): MenuItem {
	return {
		key,
		icon,
		children,
		label,
		type,
	} as MenuItem;
}

const items: any = [
	getItem('Hộp thư đến', '/mail/manage-mails/inbox', <InboxOutlined/>),
	getItem('Thư đã gửi', '/mail/manage-mails/sent', <MailOutlined/>),
	getItem('Bản thảo', '/mail/manage-mails/draft', <FileTextOutlined/>),
	getItem('Đã gắn sao', '/mail/manage-mails/starred', <StarOutlined/>),
	getItem('Đã xoá', '/mail/manage-mails/trash', <DeleteOutlined/>),
]
const MailManage = styled.div`
  .toolbar_wrap {
    float: right;
  }

  .info {
    margin-top: 150px;
  }

  .toolbar {
    min-height: 100px;
  }
`

const Starred: NextPageWithLayout = () => {
	const user = useSelector((state: RootState) => state.user);

	return (
		<MailManage>
			<Head>
				<title>Mail</title>
			</Head>
			<div className="toolbar">
				<div className="toolbar_wrap">
					<Row>
						<Select
							defaultValue={user.email}
							style={{width: 200}}
							options={[
								{value: `${user.email}`, label: `${user.email}`},
							]}
						/>
						<Input style={{width: 200, margin: "0 10px"}} placeholder="Search"/>
					</Row>
				</div>
			</div>
			<div className="info">
				<Empty/>
			</div>
		</MailManage>
	)
}
Starred.getLayout = function getLayout(page) {
	return (
		<AppLayOut>
			<MenuLayout items={items} text="Soạn tin" icon={<EditOutlined/>} keyProp="/mail/manage-mails/compose">
				{page}
			</MenuLayout>
		</AppLayOut>
	)
}
export default Starred;