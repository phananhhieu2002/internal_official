import styled from "styled-components";
import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/app-layout";
import RequestLayout from "@/layouts/request-layout";
import Head from "next/head";
import {Empty, Row} from "antd";

const RequestWrapper = styled.div`
  font-weight: 500;

  .ant-row {
    justify-content: center;
    align-items: center;
    min-height: 500px;
  }
`
const Request: NextPageWithLayout = () => {
	return (
		<RequestWrapper>
			<Head>
				<title>Request</title>
			</Head>
			<div>
				<Row>
					<Empty description="You haven't sent any requests"/>
				</Row>
			</div>
		</RequestWrapper>
	)
}
Request.getLayout = function (page) {
	return (
		<AppLayOut>
			<RequestLayout>
				{page}
			</RequestLayout>
		</AppLayOut>
	)
}
export default Request;