import * as Type from "@/constants/TaskConstants";
import {notification} from "antd";

type TypeNotification = 'warn' | 'warning' | 'info' | 'error' | 'success';
type Placement = 'topRight' | 'topLeft' | 'bottomRight' | 'bottomLeft';

class Utils {
	static showNotification = (type: TypeNotification = 'info', message = '', placement: Placement = 'topRight') => {
		// @ts-ignore
		return notification[type]({message, placement});
	};

	/*
	*  Divide string to reverse with character "-"
	* */
	static convertDate(str: any) {
		return str?.split("-").reverse().join("-");
	}

	/* Calculate count of page in Pagination
	* */
	static countPage(total: number) {
		return Math.round((total / 4) + 1);
	}

	/* Parse first character in string to Uppercase mode */
	static parseFirstCharacter(str: any) {
		return str.charAt(0).toUpperCase() + str.slice(1);
	}

	/*
	* Parse status of task to Vietnamese Status
	* */
	static parseStatus(status: string) {
		if (status === Type.TO_DO) {
			return {
				name: "Quá hạn	",
				color: "red"
			}
		} else if (status === Type.IN_PROGRESS) {
			return {
				name: "Đang triển khai",
				color: "warning"
			}
		} else if (status === Type.COMPLETED) {
			return {
				name: "Đã hoàn thành",
				color: "success"
			}
		} else {
			return {
				name: "Đang chờ",
				color: "processing"
			}
		}
	}

	/**
	 *
	 * Convert Date in Task Page
	 */
	static convertFormatDate(date: string) {
		const day = this.convertDate(date.substring(0, 10));
		const time = date.substring(11, 19);
		const hour = parseInt(time.substring(0, 2));
		var suf
		if (hour >= 0 && hour <= 12) {
			suf = "am"
		} else {
			suf = "pm";
		}
		return `${day}, ${time} ${suf}`;
	}

	static slugify = (str: any) => {
		return str
			.normalize('NFD')
			.replace(/[\u0300-\u036f]/g, '')
			.replace(/đ/g, 'd')
			.replace(/Đ/g, 'D')
			.replace(/ /g, '_')
			.toLowerCase()
			.replace(/[^\w-]+/g, '');
	};
}

export default Utils;
