import {createSlice, PayloadAction} from "@reduxjs/toolkit";

const initialState: Account[] = [];

const memberSlice = createSlice({
	name: 'member',
	initialState,
	reducers: {
		getAllMembers: (state, action: PayloadAction<Account[]>) => {
			return [
				...action.payload
			]
		}
	}
})

export const {getAllMembers} = memberSlice.actions;
export default memberSlice.reducer;