import MenuLayout from "src/layouts/menu-layout"
import {EditOutlined, SendOutlined, CommentOutlined, ArrowLeftOutlined, ArrowRightOutlined} from "@ant-design/icons";
import {MenuItem} from "@/@types/global";
import styled from "styled-components";
import {NextPageWithLayout} from "@/pages/_app";
import AppLayout from "@/layouts/app-layout";
import {useRouter} from "next/router";
import {Avatar, Button, Card, Divider, Spin} from "antd";
import React, {useEffect, useState} from "react";
import {getReportById} from "@/redux/actions/report";
import {RootState, useAppDispatch, useAppSelector} from "@/redux/store";
import {setLoading} from "@/redux/slices/report.slice";

function getItem(
	label: React.ReactNode,
	key: React.Key,
	icon?: React.ReactNode,
	children?: MenuItem[],
	type?: 'group',
): MenuItem {
	return {
		key,
		icon,
		children,
		label,
		type,
	} as MenuItem;
}

const items: any = [
	getItem('Báo cáo đã gửi', '/hrm/report/sent', <SendOutlined/>),
]

const ReportWrapper = styled.div`
  border-left: 1px solid #ccc;

  .ant-card {
    border: none;
  }

  .header {
    display: flex;
    align-items: center;
  }

  .hrm_header_title {
    font-size: 15px;
    font-weight: 500;
  }

  .count {
    font-size: 12px;
    position: relative;
    top: -5px;
  }

  .hrm_header_info {
    margin-left: 10px;
  }

  .indicate {
    margin: 10px;
  }

  .content {
    background-color: transparent;
  }

  .indicate_heading {
    border-bottom: 1px solid #333;
    font-size: 16px;
    width: fit-content;
    font-weight: 700;
    display: flex;
    align-items: center;
    padding: 3px;
  }

  .ant-card-body {
    padding: 8px 24px;
  }

  .control {
    display: flex;
    justify-content: space-between;
  }

  .ant-btn {
    display: flex;
    align-items: center;
    border-radius: 20px;
  }

  .ant-btn:hover {
    color: #333333;
  }

  .title {
    margin-left: 10px;
  }
`

const Detail: NextPageWithLayout = () => {
	const [author, setAuthor] = useState<string>("");
	const [avatar, setAvatar] = useState<string>("");
	const [content, setContent] = useState<string>("");
	const [feedback, setFeedback] = useState<string>("");
	const dispatch = useAppDispatch();
	const router = useRouter();
	const isLoading = useAppSelector((state: RootState) => state.report.isLoading);
	const getReportWithId = async () => {
		const res: any = await dispatch(getReportById(router.query.slug));
		setAuthor(res.payload.data.sender.fullName);
		setAvatar(res.payload.data.sender.avatar);
		setContent(res.payload.data.content.slice(3, res.payload.data.content.length - 4));
		setFeedback(res.payload.data.feedback);
		dispatch(setLoading(false));
	}
	useEffect(() => {
		getReportWithId();
	}, [])
	return (
		<ReportWrapper>
			<Card>
				{
					isLoading ?
						<div className="loading">
							<Spin className="spin">
								<div></div>
							</Spin>
						</div>
						:
						<div className="mail_detail">
							<div className="header">
								<div className="hrm_header_logo">
									<Avatar
										src={avatar}
									/>
								</div>
								<div className="hrm_header_info">
									<div className="other">
										<h5 className="hrm_header_title">{author}</h5>
										<span className="count">6 days ago</span>
									</div>
								</div>
							</div>
							<Divider/>
							<div className="mail_detail_content">
								<div className="indicate">
									<span className="indicate_heading">
										<EditOutlined/>
										<span className="title">
											Content :
										</span>
									</span>
								</div>
								<div className="content">{content}</div>
							</div>
							<div className="mail_detail_content">
								<div className="indicate">
									<span className="indicate_heading">
										<CommentOutlined/>
										<span className="title">
											Feedback :
										</span>
									</span>
								</div>
								<div className="content">{feedback}</div>
							</div>
						</div>
				}
				<div className="control">
					<Button onClick={() => router.replace('/hrm/report/sent')}>
						<ArrowLeftOutlined/>
						Back to List
					</Button>
					<Button>
						Reply report
						<ArrowRightOutlined/>
					</Button>
				</div>
			</Card>
		</ReportWrapper>
	)
}

Detail.getLayout = function (page) {
	return (
		<AppLayout>
			<MenuLayout items={items} icon={<EditOutlined/>} text="Viết báo cáo">
				{page}
			</MenuLayout>
		</AppLayout>
	)
}
export default Detail;    