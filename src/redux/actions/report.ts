import {createAsyncThunk} from "@reduxjs/toolkit";
import ApiService from "@/service/ApiService";

interface Queries {
	page: number,
	sortBy: string,
	range: string,
	limit: number
}

export const getReports = createAsyncThunk('reports/get', async (queries: Queries) => {
	try {
		const res = await ApiService.getSentReports(queries);
		return res;
	} catch (e) {
		console.log(e);
	}
})
export const getReportById = createAsyncThunk('report/get', async (id: any) => {
	try {
		const res = await ApiService.getReport(id);
		return res;
	} catch (e) {
		console.log(e);
	}
})