import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/app-layout";
import styled from "styled-components";
import {
	Card,
	Col,
	Row,
	Spin,
	Empty,
	Dropdown,
	Modal,
	Calendar,
	Input,
	Form,
	DatePicker,
	Upload,
	UploadProps, UploadFile, Avatar, Tag, Progress
} from "antd";
import {Typography} from 'antd';
import {
	EllipsisOutlined,
	LoadingOutlined,
	PlusOutlined,
	LogoutOutlined,
	DeleteOutlined,
	SearchOutlined
} from "@ant-design/icons";

const {Title} = Typography;
import type {MenuProps} from 'antd';
import React, {useEffect, useState} from "react";
import InputComponent from "@/components/share-components/Input";
import Head from "next/head";
import {UploadChangeParam} from "antd/lib/upload";
import {RootState, useAppDispatch, useAppSelector} from "@/redux/store";
import {createProject, deleteProject, getProjects} from "@/redux/actions/project";
import Utils from "@/utils";
import PaginationComponent from "@/components/share-components/Pagination";
import {Moment} from "moment";
import moment from "moment";

const ProjectContainer = styled.div`
  font-weight: 600;

  .ant-card-body {
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 15px;
    margin: 5px;
    border-radius: 10px;
  }

  .ant-card {
    border-radius: 18px;
    border: none;
    box-shadow: 0 0 1px #ccc;
  }

  .project {
    width: 100%;
  }

  .ant-typography {
    margin: 0;
    font-size: 16px;
    font-weight: 650 !important;
  }

  .ant-card-body:before, .ant-card-body:after {
    display: none;
  }

  .anticon-ellipsis {
    font-size: 17px;
  }

  .project_list {
    min-height: 700px;
    margin-top: 20px;
  }

  .ant-col {
    padding: 0 10px;
  }

  .ant-picker-calendar {
    margin: 20px 0;
  }

  .ant-typography {
    font-weight: 400;
  }

  .ant-input-affix-wrapper {
    padding: 8px 11px;
    border-radius: 8px;
    border: none;
    margin-bottom: 20px;
  }

  .ant-modal-body {
    padding: 24px;
  }

  .ant-form-item {
    width: 100%;
  }

  .deadline p {
    padding-bottom: 8px;
  }

  .ant-upload-wrapper {
    display: flex;
    justify-content: center;
  }

  .ant-typography {
    font-weight: 500;
  }

  .header {
    display: flex;
  }

  .header_name {
    font-weight: 700;
    font-size: 14px;
  }

  .team {
    font-weight: 400;
    font-size: 13px;
  }

  .header {
    width: 100%;
    justify-content: space-between;
  }

  .title {
    min-width: 84%;
  }

  .progress, .due {
    display: flex;
    justify-content: space-between;
  }

  .author {
    margin: 7px 0 15px;
  }

  .progress {
    margin: 13px 0;
  }

  .project_container {
    min-height: 850px;
    display: flex;
    flex-direction: column;
  }

  .title_page {
    border-radius: 6px;
  }

  .ant-pagination {
    margin-top: 40px !important;
  }

  .project_item {
    margin: 10px 0;
  }

  .ant-spin {
    top: 200px !important;
  }

  .avatar_project {
    width: 40px;
    height: 40px;
    object-fit: cover;
    border-radius: 10px;
  }
`

const Project: NextPageWithLayout = () => {
	const projects = useAppSelector((state: RootState) => state.project);
	const [id, setId] = useState<any>("");
	const page = useAppSelector((state: RootState) => state.app.page);
	const dispatch = useAppDispatch();
	const [keyword, setKeyword] = useState("");
	const [isModalOpen, setIsModalOpen] = useState(false);
	const [status, setStatus] = useState("none")
	const [loading, setLoading] = useState(false);
	const [imageUrl, setImageUrl] = useState<string>();
	const [form] = Form.useForm();
	const handleClick = (e: any) => {
		e.key === "0" ? showModal() : setStatus("flex");
	}
	const handleDelete = () => {
		Modal.confirm({
			title: "Bạn có chắc bạn muốn xóa project này không?",
			onOk: async () => {
				dispatch(deleteProject(id));
				Utils.showNotification('success', 'Xóa công việc thành công');
				try {
					//
				} catch (err) {
					console.log("==> delete request failed", err);
					Utils.showNotification('error', 'Đã có lỗi xảy ra khi xóa công việc!');
				}
			}
		})
	}
	const items: MenuProps['items'] = [{
		label: "Tạo project",
		icon: <PlusOutlined/>,
		key: '0',
		onClick: handleClick
	}, {
		label: "Tìm kiếm",
		icon: <SearchOutlined/>,
		key: '1',
		onClick: handleClick
	},
	];
	const options: MenuProps['items'] = [
		{
			label: "Rời khỏi dự án",
			icon: <LogoutOutlined/>,
			key: '0',
		},
		{
			label: "Xóa dự án",
			icon: <DeleteOutlined/>,
			key: '1',
			onClick: handleDelete
		},
	];
	const showModal = () => {
		setIsModalOpen(true);
	};

	const handleCancel = () => {
		setIsModalOpen(false);
	};
	const handleChange: UploadProps['onChange'] = (info: UploadChangeParam<UploadFile>) => {
		if (info.file.status === 'uploading') {
			setLoading(true);
			return;
		}
		if (info.file.status === 'done') {
			// Get this url from response in real world.
			// getBase64(info.file.originFileObj as RcFile, (url) => {
			// 	setLoading(false);
			// 	setImageUrl(url);
			// });
		}
	};
	const uploadButton = (
		<div>
			{loading ? <LoadingOutlined/> : <PlusOutlined/>}
			<div style={{marginTop: 8}}>Upload</div>
		</div>
	);
	const handleInput = (e: any) => {
		// console.log(e.target.value);
		setKeyword(e.target.value)
	}
	const onSubmit = (values: { name: string; description: string; dueDate: Moment }) => {
		try {
			const formData = new FormData();
			values.name && formData.append('name', values.name);
			values.description && formData.append('description', values.description);
			values.dueDate && formData.append('dueDate', moment(values.dueDate).format('x'));
			// image && formData.append('file', image);
			createProject(formData);
		} catch (error) {
			console.log(error);
		}
		setIsModalOpen(false);
	}
	useEffect(() => {
		try {
			dispatch(getProjects({page: page, slug: keyword}))
			setLoading(true);
		} catch (error) {
			console.log(error);
		}
		setTimeout(() => {
			setLoading(false);
		}, 500)
	}, [page, keyword])
	return (
		<ProjectContainer>
			<Head>
				<title>Project</title>
			</Head>
			<Row>
				<Col xs={24} lg={8}>
					<Spin
						spinning={false}
					>
						<div className="header_card">
							<Card className="title_page">
								<Title level={4}>Dự án đang tham gia</Title>
								<Dropdown menu={{items}} trigger={['click']}>
									<EllipsisOutlined/>
								</Dropdown>
								<Modal title="Tạo dự án" open={isModalOpen} onOk={() => form.submit()} onCancel={handleCancel}>
									<Form
										layout={"vertical"}
										form={form}
										onFinish={onSubmit}
									>
										<Form.Item name="avatar">
											<Upload
												// name="avatar"
												listType="picture-circle"
												className="avatar-uploader"
												showUploadList={false}
												action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
												onChange={handleChange}
											>
												{imageUrl ? <img src={imageUrl} alt="avatar" style={{width: '100%'}}/> : uploadButton}
											</Upload>
										</Form.Item>
										<Form.Item style={{margin: 0}}>
											<Row className="line_2" style={{justifyContent: "space-between"}}>
												<Col xs={24} md={12} style={{padding: "0px 8px"}}>
													<InputComponent label="Tên dự án" placeholderText="Nhập tên dự án..."/>
												</Col>
												<Col xs={24} md={12} style={{padding: "0px 8px"}}>
													<div className="deadline">
														<p style={{paddingBottom: 8}}>Hạn hoàn thành</p>
														<DatePicker style={{width: "100%"}}/>
													</div>
												</Col>
											</Row>
										</Form.Item>
										<Form.Item name="description">
											<InputComponent label="Mô tả dự án" placeholderText="Mô tả dự án..."/>
										</Form.Item>
									</Form>
								</Modal>
							</Card>
						</div>
						<div className="project_list">
							<Input style={{display: `${status}`}} onInput={handleInput} size="small" placeholder="Nhập tên dự án..."
							       prefix={<SearchOutlined/>}/>
							{
								loading ?
									<Spin tip="Đang tải dữ liệu">
										<div></div>
									</Spin> : <div>
										{
											projects.data.length > 0 ?
												<div className="project_container">
													{projects.data.map(project =>
														<Card className="project_item">
															<div className="project">
																<div className="header">
																	<div className="avatar">
																		<a href="">
																			{
																				project.image ?
																					<img className="avatar_project" src={project.image} alt="avatar_project"/>
																					:
																					<img className="avatar_project"
																					     src="https://assets.manutd.com/AssetPicker/images/0/0/12/55/800532/hercules-red-icon-1024x10241562734953094.png"
																					     alt=""/>
																			}
																		</a>
																	</div>
																	<div className="title">
																		<div className="header_name">{project.name}</div>
																		<div className="team">NS.</div>
																	</div>
																	<div className="option">
																		<Dropdown menu={{items: options}} onOpenChange={() => {
																			setId(project?.id)
																		}} trigger={['click']}>
																			<EllipsisOutlined/>
																		</Dropdown>
																	</div>
																</div>
																<div className="progress">
																	<div className="progress_point">
																		Tiến độ : {project.completedTaskCount} / 0
																	</div>
																	<div className="progress_status">
																		<Tag bordered={false} color="processing">
																			Đang tiến hành
																		</Tag>
																	</div>
																</div>
																<div className="line">
																	<Progress percent={0} showInfo={false}/>
																</div>
																<div className="author">
																	<div className="avatar">
																		<Avatar src={project.creator?.avatar}/>
																	</div>
																</div>
																<div className="due">
																	<div>
																		<Tag color="rgb(250, 173, 20)">
																			Thời hạn : {Utils.convertDate(project.dueDate?.slice(0, 10))}
																		</Tag>
																	</div>
																	<div>Quản lý</div>
																</div>
															</div>
														</Card>)}
													<PaginationComponent total={projects.pagination.total} item={3} current={page}/>
												</div>
												:
												<Empty/>
										}
									</div>
							}
						</div>
					</Spin>
				</Col>
				<Col xs={24} lg={16}>
					<Card>
						<Spin
							spinning={false}
						>
							<div className="timeline">
								<Title level={4}>Nhiệm vụ của bạn</Title>
								<Calendar/>
							</div>
						</Spin>
					</Card>
				</Col>
			</Row>
		</ProjectContainer>
	)
}

Project.getLayout = function getLayout(page) {
	return (
		<AppLayOut>
			{page}
		</AppLayOut>
	)
}

export default Project;