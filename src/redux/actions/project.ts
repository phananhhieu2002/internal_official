import {createAsyncThunk} from "@reduxjs/toolkit";
import ApiService from "@/service/ApiService";
import Utils from "@/utils";
import {message} from "antd";

interface GetProjectsQuery {
	page: number;
	slug: string;
	limit?: number;
	sortBy?: string
}

export const getProjects = createAsyncThunk<any, GetProjectsQuery>('projects/get',
	async ({
		       page = 1,
		       slug = "",
		       limit = 3,
		       sortBy = '-createdAt'
	       }) => {
		try {
			const res = await ApiService.getProjects({
				page,
				slug: Utils.slugify(slug),
				limit,
				sortBy,
			});
			return res;
		} catch (e) {
			console.log(e)
		}
	})
export const deleteProject = createAsyncThunk<any, string>('project/delete', async (id) => {
	try {
		await ApiService.deleteProject(id);
		return id;
	} catch (error) {
		console.log(error);
	}
})
export const createProject = createAsyncThunk('project/create', async (formData: any) => {
	try {
		const res = await ApiService.createProject(formData);
		message.success('Tạo project thành công')
		return res;
	} catch (error) {
		console.log(error);
	}
})