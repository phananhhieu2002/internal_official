import {Avatar} from "antd";
import {SettingOutlined} from "@ant-design/icons";
import {Typography} from 'antd';
import styled from "styled-components";
import {RootState, useAppSelector} from "@/redux/store";
import Utils from "@/utils";

const {Title} = Typography;
const HeaderContainer = styled.div`
  .infoPart {
    display: flex;
  }

  .more_info {
    font-size: 12px;
    color: var(--fadeColor);
    font-weight: 500;
    position: relative;
    bottom: 3px;
  }

  .ant-typography {
    margin: 0;
    font-weight: 700 !important;
    font-size: 13px;
  }
`
const HeaderMenu = () => {
	const user = useAppSelector((state: RootState) => state.user);
	const theme = useAppSelector((state: RootState) => state.theme.value);
	return (
		<HeaderContainer style={{display: "flex", justifyContent: "space-between", alignItems: "center"}}>
			<div className="infoPart" style={{alignItems: "center"}}>
				<Avatar style={{margin: "0 8px", width: 40, height: 40}} src={user.avatar}/>
				<div>
					<Title style={theme ? {color: "var(--light)"} : {color: "var(--dark)"}} level={5}>{user.fullName}</Title>
					<p className="more_info">{Utils.parseFirstCharacter(user.role)}</p>
				</div>

			</div>
			<span>
				<SettingOutlined style={{fontSize: 20}}/>
			</span>
		</HeaderContainer>
	)
}

export default HeaderMenu;