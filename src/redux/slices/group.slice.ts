import {createSlice} from "@reduxjs/toolkit";
import {createNewGroup, deleteGroup, getGroups} from "@/redux/actions/group";
import {Group} from "@/@types/global";

interface GroupType {
	data: Group[],
	pagination: {
		total?: number,
		page?: number,
		hasPrev?: boolean,
		hasNext?: boolean
	}
}

const initialState: GroupType = {
	data: [],
	pagination: {}
}

const groupSlice = createSlice({
	name: "group",
	initialState,
	reducers: {},
	extraReducers: builder => {
		builder.addCase(getGroups.fulfilled, (state, action) => {
			return {...state, ...action.payload}
		}),
			builder.addCase(deleteGroup.fulfilled, (state, action) => {
				state.data = state.data.filter((item) => item.id !== action.payload)
			}),
			builder.addCase(createNewGroup.fulfilled, (state, action) => {
				state.data = [...state.data, action.payload.data]
			})
	}
})


export const {} = groupSlice.actions;
export default groupSlice.reducer;