import {createSlice} from "@reduxjs/toolkit";
import {deleteProject, getProjects} from "../actions/project";
import {Project} from "@/@types/global";

interface projectState {
	data: Project[],
	pagination: {
		hasNext?: boolean,
		hasPrev?: boolean,
		page: number,
		total: number
	}
}

const initialState: projectState = {
	data: [],
	pagination: {
		page: 1,
		total: 0
	}
}

const projectSlice = createSlice({
	name: "project",
	initialState,
	reducers: {},
	extraReducers: builder => {
		builder.addCase(getProjects.fulfilled, (state, action) => {
			return {...state, ...action.payload}
		}),
			builder.addCase(deleteProject.fulfilled, (state, action) => {
				state.data = state.data.filter(item => item.id !== action.payload);
			})
		// builder.addCase(createProject.fulfilled, (state, action) => {
		// 	state.data = [...state.data, action.payload.data]
		// })
	}
})

export const {} = projectSlice.actions;
export default projectSlice.reducer;