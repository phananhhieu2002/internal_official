import {TokenType} from "@/@types/global";
import {APP_TOKEN_KEY} from "@/configs/app.config";
import ApiService from "@/service/ApiService";
import {getData, setData} from "@/service/storage.service";
import {createAsyncThunk} from "@reduxjs/toolkit";

export const signOut = createAsyncThunk('auth-components/signOut', async _ => {
	const {refresh_token} = await getData<TokenType>(APP_TOKEN_KEY, {
		access_token: null,
		refresh_token: null,
	});
	if (refresh_token) {
		await ApiService.logout(refresh_token.token);
	}
	await setData(APP_TOKEN_KEY, null);
	await setData('isLoggedIn', false);
	return _;
});