import AppLayOut from "@/layouts/app-layout";
import {NextPageWithLayout} from "../_app";
import styled from 'styled-components';
import {Table} from "antd";
import type {ColumnsType} from 'antd/es/table';
import React from "react";
import TaskLayout from "src/layouts/task-layout";

interface DataType {
	key?: string;
	title?: string;
	firstName: string;
	lastName: string;
	age: number;
	address: string;
	tags: string[];
}

const columns: ColumnsType<DataType> = [
	{
		title: 'Tiêu đề',
		dataIndex: 'name',
		key: 'name',
	},
	{
		title: 'Thời hạn',
		dataIndex: 'age',
		key: 'age',
	},
	{
		title: 'Trạng thái',
		dataIndex: 'address',
		key: 'address',
	},
	{
		title: 'Người tham gia',
		key: 'tags',
		dataIndex: 'tags',
	},
	{
		title: 'Hành động',
		key: 'action',
	},
];
const data: DataType[] = [
	{
		key: '3',
		firstName: 'Joe',
		lastName: 'Black',
		age: 32,
		address: 'Sydney No. 1 Lake Park',
		tags: ['cool', 'teacher'],
	},
];
const MainContent = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

const Archived: NextPageWithLayout = () => {
	return (
		<MainContent>
			<Table columns={columns} dataSource={data}/>
		</MainContent>
	)
}
Archived.getLayout = function getLayout(page) {
	return (
		<AppLayOut>
			<TaskLayout>
				{page}
			</TaskLayout>
		</AppLayOut>
	)
}

export default Archived;