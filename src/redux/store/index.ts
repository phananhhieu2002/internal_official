import {configureStore} from "@reduxjs/toolkit";
import themeReducer from "@/redux/slices/theme.slice";
import appReducer from "@/redux/slices/app.slice";
import authReducer from "@/redux/slices/auth.slice";
import userReducer from "@/redux/slices/user.slice";
import memberReducer from "@/redux/slices/member.slice";
import taskReducer from "@/redux/slices/tasks.slice";
import groupReducer from "@/redux/slices/group.slice";
import projectReducer from "@/redux/slices/project.slice";
import reportReducer from "@/redux/slices/report.slice";
import {TypedUseSelectorHook, useDispatch, useSelector} from "react-redux";
export const store = configureStore({
  reducer : {
    auth : authReducer,
    app : appReducer,
    theme : themeReducer,
    user : userReducer,
    member : memberReducer,
    task : taskReducer,
    group : groupReducer,
    project : projectReducer,
    report : reportReducer
  }
})

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

export const useAppDispatch: () => AppDispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;