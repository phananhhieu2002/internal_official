import {createSlice} from "@reduxjs/toolkit";
import {deleteTask, getTasks, getTasksByName} from "../actions/task";
import {Task} from "@/@types/global";

interface taskType {
	data: Task[],
	pagination: {
		total: number,
		page?: number,
		hasPrev?: boolean,
		hasNext?: boolean
	}
}

const initialState: taskType = {
	data: [],
	pagination: {
		total: 0,
		page: 1,
	}
}

const tasksSlice = createSlice({
	name: "task",
	initialState,
	reducers: {},
	extraReducers: builder => {
		builder.addCase(getTasks.fulfilled, (state, action: any) => {
			return action.payload;
		}),
			builder.addCase(deleteTask.fulfilled, (state, action) => {
				state.data = state.data.filter((item) => item.id !== action.payload)
			}),
			builder.addCase(getTasksByName.fulfilled, (state, action) => {
				return {...state, ...action.payload}
			})
	}
})


export const {} = tasksSlice.actions;
export default tasksSlice.reducer;