const dev = {
	// API_ENDPOINT_URL: 'https://stable.hermess.site/api/v1',
	API_ENDPOINT_URL: 'https://stable-api.northstudio.dev/api/v1',
	// API_ENDPOINT_URL: 'https://api.u2u.site/api/v1',
};

const prod = {
	// API_ENDPOINT_URL: "http://localhost:6688/api/v1",
	API_ENDPOINT_URL: 'https://api.u2u.site/api/v1',
};

const test = {
	API_ENDPOINT_URL: 'https://api.test.com',
};

const getEnv = () => {
	console.log(process.env.NODE_ENV);
	switch (process.env.NODE_ENV) {
		case 'development':
			return dev;
		case 'production':
			return prod;
		case 'test':
			return test;
		default:
			break;
	}
};

export const env = getEnv();
