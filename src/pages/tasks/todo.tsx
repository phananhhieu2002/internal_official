import AppLayOut from "@/layouts/app-layout";
import {NextPageWithLayout} from "../_app";
import styled from 'styled-components';
import {Table} from "antd";
import type {ColumnsType} from 'antd/es/table';
import React, {ReactElement} from "react";
import TaskLayout from "src/layouts/task-layout";

interface User {
	name: string;
	avatar: string;
}

interface DataType {
	key: string;
	title: string;
	status: ReactElement;
	user?: User;
}

const columns: ColumnsType<DataType> = [
	{
		title: 'Tiêu đề',
		dataIndex: 'name',
		key: 'name',
	},
	{
		title: 'Thời hạn',
		dataIndex: 'age',
		key: 'age',
	},
	{
		title: 'Trạng thái',
		dataIndex: 'address',
		key: 'address',
	},
	{
		title: 'Người tham gia',
		key: 'tags',
		dataIndex: 'tags',
	},
	{
		title: 'Hành động',
		key: 'action',
	},
];
const data: DataType[] = [
	{
		key: '1',
		title: '[Sabo MMO] feat/subwebsite',
		status: <span className="ant-tag ant-tag-red">Quá hạn</span>,
	},
];
const MainContent = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

const Todo: NextPageWithLayout = () => {
	return (
		<MainContent>
			<Table columns={columns} dataSource={data}/>
		</MainContent>
	)
}
Todo.getLayout = function getLayout(page) {
	return (
		<AppLayOut>
			<TaskLayout>
				{page}
			</TaskLayout>
		</AppLayOut>
	)
}

export default Todo;