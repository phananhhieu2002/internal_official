import styled from "styled-components";
import {Radio} from "antd";
import {RootState, useAppDispatch, useAppSelector} from "@/redux/store";
import {dark, light} from "@/redux/slices/app.slice";

const CategoryWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 1.5rem 0;
`
const CategoryColor = () => {
    const ColorCategory = useAppSelector((state : RootState) => state.app.color);
    const dispatch = useAppDispatch();
    const options = [
        { label: 'Light', value: 'Light' },
        { label: 'Dark', value: 'Dark' },
    ];
    const handleChange = () => {
        ColorCategory === 'Light' ? dispatch(dark()) : dispatch(light());
    }
    return (
        <CategoryWrapper>
            <div className="settingsLabel">Side Nav Color:</div>
            <div className="switchWrapper">
                <Radio.Group options={options} defaultValue={ColorCategory} onChange={handleChange} optionType="button" />
            </div>
        </CategoryWrapper>
    )
}
export default CategoryColor;