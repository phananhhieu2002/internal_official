import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {Report} from "@/@types/global";
import {getReports} from "@/redux/actions/report";

interface ReportData {
	data: Report[],
	pagination: {
		hasNext?: boolean,
		hasPrev?: boolean,
		page: number,
		total: number
	},
	isLoading: boolean
}

const initialState: ReportData = {
	data: [],
	pagination: {
		page: 1,
		total: 0
	},
	isLoading: true
}

const reportSlice = createSlice({
	name: "report",
	initialState,
	reducers: {
		setLoading: (state, action) => {
			state.isLoading = action.payload;
		}
	},
	extraReducers: builder => {
		builder.addCase(getReports.fulfilled, (state, action: PayloadAction<any>) => {
			return {...action.payload, isLoading: false};
		})
	}
})

export const {setLoading} = reportSlice.actions;
export default reportSlice.reducer;