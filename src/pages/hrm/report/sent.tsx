import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/app-layout";
import MenuLayout from "src/layouts/menu-layout";
import React, {useEffect} from "react";
import {EditOutlined, SendOutlined, LinkOutlined, CalendarOutlined} from "@ant-design/icons";
import styled from "styled-components";
import {Card, Row, Col, Select, Avatar, Empty, Spin} from "antd";
import {useRouter} from "next/router";
import {getReports} from "@/redux/actions/report";
import {RootState, useAppDispatch, useAppSelector} from "@/redux/store";
import PaginationComponent from "@/components/share-components/Pagination";
import {setLoading} from "@/redux/slices/report.slice";
import {MenuItem} from "@/@types/global";

function getItem(
	label: React.ReactNode,
	key: React.Key,
	icon?: React.ReactNode,
	children?: MenuItem[],
	type?: 'group',
): MenuItem {
	return {
		key,
		icon,
		children,
		label,
		type,
	} as MenuItem;
}

const items: any = [
	getItem('Báo cáo đã gửi', '/hrm/report/sent', <SendOutlined/>),
]
const SentContainer = styled.div`
  font-weight: 500;

  .sent_content {
    min-height: 500px;
    margin-top: 30px;
  }

  .ant-empty {
    margin: auto;
  }

  .report:hover {
    cursor: pointer;
  }

  .report:hover .content {
    background-color: #e9e9e9;
  }

  .ant-col {
    padding: 0 9px;
  }

  .ant-card-body {
    padding: 15px 20px;
  }

  .ant-card {
    width: 431px;
    /* height: 200px; */
    border: none;
    box-shadow: 1px 1px 2px #ccc;
  }

  .header {
    justify-content: space-between;
  }

  .txt {
    margin-left: 10px;
    font-weight: 700;
  }

  .content {
    margin: 20px 0;
    padding: 10px;
    width: 100%;
    border-radius: 10px;
    height: 100px;
  }

  .detail {
    display: flex;
    justify-content: space-between;
  }

  .create_at {
    display: flex;
    align-items: center;
    color: #ccc;
  }

  .create_time {
    margin-left: 7px;
  }

  .heading {
    font-size: 18px;
    font-weight: 700;
  }

  .content {
    font-size: 16px;
  }

  .loading {
    width: 100%;
    display: flex;
  }

  .ant-spin-nested-loading {
    margin: auto;
  }
`
const Sent: NextPageWithLayout = () => {
	const router = useRouter();
	const page = useAppSelector((state: RootState) => state.app.page);
	/* Chinh isloading vao object */
	const reports = useAppSelector((state: RootState) => state.report);
	const isLoading = reports.isLoading;
	const dispatch = useAppDispatch();
	const handleClick = (id: any) => {
		router.replace(`/hrm/report/${id}`)
		dispatch(setLoading(true));
	}
	useEffect(() => {
		const payload = {page: 1, sortBy: '-createdAt', range: 'createdAt', limit: 9};
		dispatch(getReports(payload));
	}, []);
	return (
		<SentContainer>
			<Row className="header">
				<h1 className="heading">Báo cáo đã gửi</h1>
				<Select
					style={{width: 120}}
					defaultValue="week"
					options={[
						{value: 'week', label: 'Tuần qua'},
						{value: 'month', label: 'Tháng qua'},
						{value: 'alltime', label: 'Tất cả'}
					]}
				/>
			</Row>
			<Row className="sent_content">
				{
					isLoading ?
						<div className="loading">
							<Spin className="spin">
								<div></div>
							</Spin>
						</div>
						:
						<>
							{
								reports.data.length > 0 ? reports.data.map(report => (
										<Col onClick={() => {
											handleClick(report.id)
										}}>
											<Card className="report">
												<div className="header">
													<div className="time_report">
														<LinkOutlined/>
														<span className="txt">Report week 40</span>
													</div>
												</div>
												<div className="content">
													{report.content.slice(3, report.content.length - 4)}
												</div>
												<div className="detail">
													<div className="avatar">
														<Avatar src={report.sender.avatar}/>
													</div>
													<div className="create_at">
														<CalendarOutlined/>
														<span className="create_time">18 minutes ago</span>
													</div>
												</div>
											</Card>
										</Col>
									))
									: <Empty/>
							}
						</>
				}

			</Row>
			<PaginationComponent current={page} item={9} total={reports.pagination.total}/>
		</SentContainer>
	)
}

Sent.getLayout = function getLayout(page) {
	return (
		<AppLayOut>
			<MenuLayout items={items} icon={<EditOutlined/>} text="Viết báo cáo" keyProp="/hrm/report/new">
				{page}
			</MenuLayout>
		</AppLayOut>
	)
}
export default Sent;