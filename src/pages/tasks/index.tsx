import AppLayOut from "@/layouts/app-layout";
import {NextPageWithLayout} from "../_app";
import styled from 'styled-components';
import {Avatar, Button, Modal, Space, Spin, Table, Tag} from "antd";
import React, {useEffect, useMemo, useState} from "react";
import TaskLayout from "src/layouts/task-layout";
import Head from "next/head";
import {DeleteOutlined, EditOutlined} from "@ant-design/icons";
import {RootState, useAppDispatch, useAppSelector} from "@/redux/store";
import Utils from "@/utils";
import PaginationComponent from "@/components/share-components/Pagination";
import TaskForm from "@/components/tasks/TaskForm";
import {setIsModalOpen} from "@/redux/slices/app.slice";
import {deleteTask, getTasks} from "@/redux/actions/task";

const {Column} = Table;

const MainContent = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  width: 100%;

  .ant-table {
    font-weight: 500 !important;
  }

  .ant-table-tbody {
    font-size: 13px !important;
  }

  .ant-btn {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .loading {
    display: flex;
    align-items: center;
    justify-content: center;
  }
`


const Index: NextPageWithLayout = () => {
	const tasks = useAppSelector((state: RootState) => state.task);
	const page = useAppSelector((state: RootState) => state.app.page);
	const isModalOpen = useAppSelector((state: RootState) => state.app.isModalOpen);
	const [id, setId] = useState<any>(null);
	const [total, setTotal] = useState(tasks.pagination.total);
	const [loading, setLoading] = useState(false);
	const [open, setOpen] = useState(isModalOpen);
	const dispatch = useAppDispatch();
	const data = useMemo(() => {
		return tasks.data.map(task => {
			return {
				id: task.id,
				name: task.name,
				due: Utils.convertFormatDate(String(task.dueDate)),
				status: Utils.parseStatus(task.state as string),
				member: task.creator && task.creator.avatar
			}
		});
	}, [tasks]);
	const showModal = (item: any) => {
		setId(item.id);
		setOpen(true);
		dispatch(setIsModalOpen(true))
	};
	const handleCancel = () => {
		setOpen(false);
		dispatch(setIsModalOpen(false))
	};
	useEffect(() => {
		setLoading(true);
		const payload = {page, sortBy: '-createdAt'}
		dispatch(getTasks(payload))
		setTimeout(() => {
			setLoading(false)
		}, 1000)
	}, [page])
	useEffect(() => {
		setTotal(tasks.pagination.total);
	}, [tasks])
	useEffect(() => {
		setOpen(isModalOpen)
	}, [isModalOpen])

	return (
		<MainContent>
			<Head>
				<title>Task</title>
			</Head>
			{
				loading ?
					<div className="loading">
						<Spin size="large"/>
					</div>
					:
					<>
						<Table pagination={false} dataSource={data}>
							<Column title="Tiêu đề" dataIndex="name" key="name"/>
							<Column title="Thời hạn" dataIndex="due" key="due"/>
							<Column
								title="Trạng thái"
								key="status"
								render={(_: any, item: any) => (
									<Space size="middle">
										<Tag bordered={false} color={item.status.color}>
											{item.status.name}
										</Tag>
									</Space>
								)}
							/>
							<Column
								title="Người tham gia"
								key="member"
								render={(_: any, item: any) => (
									<Space size="middle">
										<Avatar src={item.member}/>
									</Space>
								)}
							/>
							<Column
								title="Hành động"
								key="action"
								render={(item: any) => (
									<Space size="middle">
										<Button
											icon={<EditOutlined/>}
											size={'middle'}
											onClick={() => {
												showModal(item)
											}}
										/>
										<Button
											type="primary"
											danger
											icon={<DeleteOutlined/>}
											onClick={() => {
												Modal.confirm({
													title: "Bạn có chắc bạn muốn xóa task này không?",
													onOk: async () => {
														dispatch(deleteTask(item.id));
														Utils.showNotification('success', 'Xóa công việc thành công');
														try {
															//
														} catch (err) {
															console.log("==> delete request failed", err);
															Utils.showNotification('error', 'Đã có lỗi xảy ra khi xóa công việc!');
														}
													}
												})
											}}
										>
										</Button>
									</Space>
								)}
							/>
						</Table>
						<Modal
							open={open}
							onCancel={handleCancel}
							style={{minWidth: 1200}}
							footer={null}
						>
							<TaskForm title="Chỉnh sửa nhiệm vụ" id={id}/>
						</Modal>
						<PaginationComponent total={total} item={tasks.data.length} current={page}/>
					</>
			}

		</MainContent>
	)
}
Index.getLayout = function getLayout(page) {
	return (
		<AppLayOut>
			<TaskLayout>
				{page}
			</TaskLayout>
		</AppLayOut>
	)
}

export default Index;