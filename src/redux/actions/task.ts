import ApiService from "@/service/ApiService";
import {TaskFilter} from "@/@types/global";
import {createAsyncThunk} from "@reduxjs/toolkit";

interface Queries {
	page: number,
	sortBy: string
}

export const getTasks = createAsyncThunk('tasks/gets', async (queries: Queries) => {
	const {page, sortBy} = queries;
	const taskFilter: TaskFilter = {};
	const res = await ApiService.getGeneralTasks({
		...taskFilter,
		page,
		sortBy,
		limit: 10
	})
	console.log(res);
	return res;
})
export const deleteTask = createAsyncThunk('tasks/delete', async (id: string) => {
	await ApiService.deleteTask(id);
	return id;
})

export const getTasksByName = createAsyncThunk('tasks/getByName', async (name: string) => {
	const res = await ApiService.getTasks({fullName: name});
	return res;
})