import CardTypo from "@/components/share-components/CardTypo/CardTypo";
import Task from "@/components/share-components/Task/Task";
import { Card, Col, Empty, Row } from "antd";
import AppLayOut from "@/layouts/app-layout";
import { NextPageWithLayout } from "@/pages/_app";
import styled from 'styled-components';
import RankComponent from "src/components/home_component/rank";
import moment from 'moment';
import TemperatureComponent from "@/components/home_component/Temperature";
import Head from "next/head";
import { useEffect, useState } from "react";
import ApiService from "@/service/ApiService";
import {RootState, useAppDispatch} from "@/redux/store";
import {useSelector} from "react-redux";
import {getTasksByName} from "@/redux/actions/task";

const HomePage = styled.div`
.ant-card-head-title{
  font-weight: 700;
  font-size: 14px;
}
  .ant-row{
    flex-wrap: nowrap;
  }
  .inside{
    padding: 0 !important;
    background-color: transparent !important;
  }
  .row-2{
    margin-top: 20px;
  }
  .row-2 .ant-col{
    padding-right: 8px;
  }
  .ant-card-extra a{
    color: rgb(62, 121, 247);
    font-size: 13px;
    font-weight: 600;
  }
  .item{
    padding: 0;
  }
  .history_exit{
    min-height: 270px;
  }
  .ant-empty-description{
    font-weight: 450;
  }
`


const Home: NextPageWithLayout = () => {
  const tasks = useSelector((state: RootState) => state.task.data);
  const user = useSelector((state: RootState) => state.user);
  const dispatch = useAppDispatch();
  const [entranceHistory, setEntranceHistory] = useState<any>(null);
  const [todo, setTodo] = useState<string | number>("...");
  const [inProgress, setInProgress] = useState<string | number>("...");
  const [completed, setCompleted] = useState<string | number>("...");
  const [quote, setQuote] = useState('""""-');
  const currentDate = moment().format("MMM Do");
  useEffect(() => {
    ApiService.getRandomQuote()
      .then(res => {
        setQuote(`"${res[0].q}" - ${res[0].a}`);
      })
  }, [])
  useEffect(() => {
    ApiService.loadSelfEntranceHistory({
      limit: 5,
      sortBy: '-time',
    })
      .then(response => {
        const history = response.data;
        setEntranceHistory(history);
      })
      .catch(() => {
        console.log("Error");
      })
  }, [])
  useEffect(() => {
    dispatch(getTasksByName(user.fullName as string));
  }, []);
  useEffect(() => {
    const value = [0, 0, 0];
    tasks.length > 0 && tasks.forEach(task => {
      if (task.state === "TODO") {
        value[0]++;
      }
      else if (task.state === "IN_PROGRESS") {
        value[1]++;
      }
      else {
        value[2]++;
      }
    })
    setTodo(value[0]);
    setInProgress(value[1]);
    setCompleted(value[2]);
  }, [tasks])
  return (
    <HomePage className="inside">
      <Head>
        <title>Internal</title>
      </Head>
      <Row>
        <Col span={18}>
          <Row className="row-1">
            <Col className="item item_survive" xs={24} sm={24} md={24} lg={24} xl={8}>
              <Task
                name="Công việc tồn đọng"
                amount={`${todo} tasks`}
                process="Công việc chưa được xử lý"
                color="rgb(255, 107, 114)"
              />
            </Col>
            <Col className="item item_processing" xs={24} sm={24} md={24} lg={24} xl={8}>
              <Task
                name="Công việc đang tiến hành"
                amount={`${inProgress} tasks`}
                process="Công việc đang được xử lý"
                color="rgb(250, 173, 20)"
              />
            </Col>
            <Col className="item item_success" xs={24} sm={24} md={24} lg={24} xl={8}>
              <Task
                name="Công việc đã hoàn thành"
                amount={`${completed} tasks`}
                process="Công việc đã được xử lý"
                color="rgb(0, 171, 111)"
              />
            </Col>
          </Row>
          <Row className="row-2">
            <Col xs={24} sm={24} md={24} lg={24} xl={16}>
              <CardTypo
                name={currentDate}
                text={quote}
              />
            </Col>
            <Col xs={24} sm={24} md={24} lg={24} xl={8}>
              <Card className="history_exit" type="inner" title="Lịch sử ra vào" extra={<a href="">Xem tất cả</a>}>
                {
                  entranceHistory && entranceHistory.length > 0 ? <></> : <Empty description="You have no activities" image={Empty.PRESENTED_IMAGE_SIMPLE} />
                }
              </Card>
            </Col>
          </Row>
        </Col>
        <Col span={6}>
          <RankComponent />
          <Row>
            <TemperatureComponent />
          </Row>
        </Col>
      </Row>
    </HomePage>
  )
}
Home.getLayout = function getLayout(page) {
  return (
    <AppLayOut>{page}</AppLayOut>
  )
}

export default Home;


